INC=-I./upb -I./upb/generated_for_cmake
CFLAGS=-g
BINDINGS_SRC=$(wildcard bindings/*c)
UPB_SRC+=$(wildcard upb/*.c)
UPB_SRC+=$(wildcard upb/upb/*.c)
UPB_SRC+=$(wildcard upb/upb/pb/*.c)
UPB_SRC+=$(wildcard upb/upb/google/protobuf/*.c)
PROTOBUF_SRC=$(wildcard upb/generated_for_cmake/google/protobuf/*.c)

SRC=$(BINDINGS_SRC) $(UPB_SRC) $(PROTOBUF_SRC)

all:
	$(CC) $(CFLAGS) $(INC) $(SRC) main.c -o repro
