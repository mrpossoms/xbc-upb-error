/*
 * The Frame eXchange Binary (`.fxb`) file definition.
 *
 * This file allows storing frame data for celestial and spacecraft bodies, along with optional
 * named parameters of said object. A frame is defined by an optional translation via an EXB, and
 * an optional rotation via an AXB. Each frame is defined over the applicable time interval of
 * either the EXB or the AXB, whichever has the smaller time interval. The program must support both
 * EXB and AXB loading to correctly represent frames defined in the FXB. Optionally, the program
 * must also support other *XB files depending the frame definition requirements.
 */
syntax = "proto3";

package xb;

import public "common.proto";

message FrameContainer {
  Metadata meta = 1;
  map<string, Parameter> parameters = 2;
  repeated Frame frames = 3;
}


message Frame {
  message Identifier {
    /* All objects are given an Identifier. Identifiers must be have either a non-zero number
     * and/or a non-empty string name. The number may be the NAIF_ID, or another identifier
     * which is understood by the publisher and user of the *XB file. The name may be used to store
     * the SPACEWARN identifier, or another string understood by both the publisher and the user.
     */
    sint32 number = 1;
    string name = 2;
  }

  // Unique identifier of the frame
  Identifier id = 1;
  // Unique identifier of the *XB for the translation component (EXB).
  Identifier exb_id = 2;
  // Unique identifier of the *XB for the rotation component (AXB).
  Identifier axb_id = 3;
  // An optional map of parameter name to parameter value and unit.
  map<string, Parameter> parameters = 4;
}
