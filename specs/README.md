# XB

# Data files?
Go in the [data](./data) folder of this repo for the latest DE438s and DE436s EXB and FXB files.

# FAQ
1. _Who can use XB files?_ For the time being, each application outside of Advanced Space needs requires explicit approval to use these files. 
1. _How to use XB implementations?_ Head over to the [`xb` group](https://gitlab.com/AdvancedSpace/xb/) and use whichever implementation you need as a submodule.
1. _How can I store maneuver information in EXB files?_ The best is probably to add `dv_x`, `dv_y`, `dv_z` as parameters to an EXB State. If this isn't sufficient, a later version could add a optional Maneuver field (of type `Vector` with unit).

# Summary of performance
Slightly smaller files (530 kB for de436s and de438s), and significantly faster access times!

Average over three runs on a laptop with an Intel i7 processor (8 hyperthreaded cores).

  Case | For bodies, for times  | For times, for bodies
--|---|---|
 EXB performance improvement |  6.45 | 5.32
 BSP times (s) Python |  5.39 | 5.43
 EXB times (s) Python |  0.83 | 1.02
 EXB times Rust (s)  | **0.011** |  **0.007**

# Goal
The goal of this project is to introduce to the aerospace community a new file format as a standard to exchanging astrodynamic-related information. Such information pertains to ephemeris of celestial bodies and spacecraft, the past and future attitude information of spacecraft and their instruments, and optionally other useful parameters tied to each of these objects (gravitational parameters, maneuvers, reference frame definitions, mathematical comments, etc.).

A smaller size is not a requirement. We focused on easy parsing, easy writing, clarity in the data being read, and verification of that data at reading time. Protobufs will fail to decode if something is wrong. On the contrary, BSPs will load correctly and it's up to the implementer to be sure that the parsing is correct.

The purpose of gaining large adoption of this file format is to make an imprint in the engineering community. Open source projects also function as effective recruiting tools. We are proposing a file format which is modern, based on open-source tools, has more features than the 1980s DAF/SPK/CK files, and which will be used internally. Making is a de-facto standard would increase the potential valuation of our in-house tools and of our engineering team.

# Identified need
The de-facto standard is SPICE SPK and CK files. These are in effect FORTRAN-77 memory dumps. [src](https://naif.jpl.nasa.gov/pub/naif/toolkit_docs/FORTRAN/req/daf.html#Array%20Addresses) They are complicated enough to read and write to that only two libraries are available to the public which can provide read and write features, despite many aborted attempts efforts by reasonably good software engineers. 

Furthermore, these files inconsistently store useful information in the  comments, such as  gravitational parameters and start and stop dates of the ephemeris. They have other limitations such as the inability  to store covariance information. In the case of the CK or attitude kernel, all times are fixed to a “spacecraft clock” which makes it difficult to convert back to a commonly used timestamp. As mentioned in the official documentation on the spacecraft clock, these represent an outdated manner of keeping time onboard spacecraft. [src](https://naif.jpl.nasa.gov/pub/naif/toolkit_docs/C/req/ck.html#Spacecraft%20Clock%20Time) It also unnecessarily complicates simulations as there is a need to support this obsolete clock management system.

The need at Advanced Space for a redefinition of these files arose from our work on CAPS and with another customer. For the former project, we must communicate to each CAPS participant the future position, velocity and related covariance of the next partner in the network in order to initialize the filters correctly. For the latter, we had to perform some proximity analysis of a set of vehicles deployed by a single launch.

The need for a replacement of SPK files isn’t new within the industry. For example, GMAT 2018a added the support to export ephemeris data in “Code500”, an unspecified ephemeris file format used internally at Goddard.
