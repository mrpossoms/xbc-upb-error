#include "bindings/nxb.upb.h"

#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>


#define NXB_ARENA_SIZE (1 << 25)

upb_arena* 		nxb_arena;
uint8_t 		arena_buf[NXB_ARENA_SIZE];

xb_NavigationContainer* load_nxb(const char* path)
{
  const uint8_t* buffer;

  int fd = open(path, O_RDONLY);
  off_t file_size = lseek(fd, 0, SEEK_END);
  lseek(fd, 0, SEEK_SET);

  buffer = mmap(NULL, file_size, PROT_READ, MAP_PRIVATE, fd, 0);

  nxb_arena = upb_arena_init(&arena_buf, NXB_ARENA_SIZE, NULL);
  
  return xb_NavigationContainer_parse((const char*)buffer, file_size, nxb_arena);
}


int main (int argc, const char* argv[])
{
	xb_NavigationContainer* nxb_container = load_nxb("./msr_stone_lro_9m.nxb");

	assert(NULL != nxb_container);

	size_t object_count = 0;
	const xb_TrackedObject* const* objects = xb_NavigationContainer_objects(nxb_container, &object_count);
	assert(object_count > 0);
	assert(NULL != objects);

	size_t pass_count = 0;
	const xb_TrackingPass* const*  passes = xb_TrackedObject_passes(objects[0], &pass_count);
	assert(NULL != passes);
	assert(pass_count > 0);

	const struct xb_EphemRegistry* registery = xb_TrackingPass_records(passes[0]);
	assert(NULL != registery);

	size_t time_index_count = 0;
	uint32_t const* time_indices = xb_EphemRegistry_time_index(registery, &time_index_count);
	assert(time_index_count > 0);
	assert(NULL != time_indices);

	return 0;
}