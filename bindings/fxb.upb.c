/* This file was generated by upbc (the upb compiler) from the input
 * file:
 *
 *     fxb.proto
 *
 * Do not edit -- your changes will be discarded when the file is
 * regenerated. */

#include <stddef.h>
#include "upb/msg.h"
#include "fxb.upb.h"
#include "common.upb.h"

#include "upb/port_def.inc"

static const upb_msglayout *const xb_FrameContainer_submsgs[3] = {
  &xb_Frame_msginit,
  &xb_FrameContainer_ParametersEntry_msginit,
  &xb_Metadata_msginit,
};

static const upb_msglayout_field xb_FrameContainer__fields[3] = {
  {1, UPB_SIZE(0, 0), 0, 2, 11, 1},
  {2, UPB_SIZE(4, 8), 0, 1, 11, 4},
  {3, UPB_SIZE(8, 16), 0, 0, 11, 3},
};

const upb_msglayout xb_FrameContainer_msginit = {
  &xb_FrameContainer_submsgs[0],
  &xb_FrameContainer__fields[0],
  UPB_SIZE(12, 24), 3, false,
};

static const upb_msglayout *const xb_FrameContainer_ParametersEntry_submsgs[1] = {
  &xb_Parameter_msginit,
};

static const upb_msglayout_field xb_FrameContainer_ParametersEntry__fields[2] = {
  {1, UPB_SIZE(0, 0), 0, 0, 9, 1},
  {2, UPB_SIZE(8, 16), 0, 0, 11, 1},
};

const upb_msglayout xb_FrameContainer_ParametersEntry_msginit = {
  &xb_FrameContainer_ParametersEntry_submsgs[0],
  &xb_FrameContainer_ParametersEntry__fields[0],
  UPB_SIZE(16, 32), 2, false,
};

static const upb_msglayout *const xb_Frame_submsgs[4] = {
  &xb_Frame_Identifier_msginit,
  &xb_Frame_ParametersEntry_msginit,
};

static const upb_msglayout_field xb_Frame__fields[4] = {
  {1, UPB_SIZE(0, 0), 0, 0, 11, 1},
  {2, UPB_SIZE(4, 8), 0, 0, 11, 1},
  {3, UPB_SIZE(8, 16), 0, 0, 11, 1},
  {4, UPB_SIZE(12, 24), 0, 1, 11, 4},
};

const upb_msglayout xb_Frame_msginit = {
  &xb_Frame_submsgs[0],
  &xb_Frame__fields[0],
  UPB_SIZE(16, 32), 4, false,
};

static const upb_msglayout_field xb_Frame_Identifier__fields[2] = {
  {1, UPB_SIZE(0, 0), 0, 0, 17, 1},
  {2, UPB_SIZE(4, 8), 0, 0, 9, 1},
};

const upb_msglayout xb_Frame_Identifier_msginit = {
  NULL,
  &xb_Frame_Identifier__fields[0],
  UPB_SIZE(16, 32), 2, false,
};

static const upb_msglayout *const xb_Frame_ParametersEntry_submsgs[1] = {
  &xb_Parameter_msginit,
};

static const upb_msglayout_field xb_Frame_ParametersEntry__fields[2] = {
  {1, UPB_SIZE(0, 0), 0, 0, 9, 1},
  {2, UPB_SIZE(8, 16), 0, 0, 11, 1},
};

const upb_msglayout xb_Frame_ParametersEntry_msginit = {
  &xb_Frame_ParametersEntry_submsgs[0],
  &xb_Frame_ParametersEntry__fields[0],
  UPB_SIZE(16, 32), 2, false,
};

#include "upb/port_undef.inc"

